﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab4Wpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Входные данные НДКА
        /// </summary>
        Dictionary<string, Dictionary<int, string>> CommandsNDFA;
        string[] Z_NDFA, Q_NDFA;
        string StartNDFA;

        /// <summary>
        /// Выходные данные ДКА
        /// </summary>
        Dictionary<string, Dictionary<int, string>> CommandsDFA;
        string Z_DFA, StartDFA;
        List<string> Q_DFA;

        public MainWindow()
        {
            InitializeComponent();

            /// Заполняем Входные данные НДКА значениями
            //textCommand.Text = "A-1-A,B\r\nA-2-A\r\nB-2-A,B";
            //textZ.Text = "1,2";
            //textQ.Text = "A,B";
            //textStart.Text = "A";
            textCommand.Text = "A-1-A,B\r\nA-2-A,B,C\r\nB-1-A\r\nB-2-B\r\nB-3-C";
            textZ.Text = "1,2,3";
            textQ.Text = "A,B,C";
            textStart.Text = "A";

            btnPrepare.Click += BtnPrepare_Click;
        }

        private void BtnPrepare_Click(object sender, RoutedEventArgs e)
        {
            /// Считываем входные данные НДКА
            Z_NDFA = textZ.Text.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            Q_NDFA = textQ.Text.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            CommandsNDFA = InitialCommand(textCommand.Text, Z_NDFA, Q_NDFA);
            StartNDFA = textStart.Text;

            CommandsDFA = new Dictionary<string, Dictionary<int, string>>();

            /// TODO
            AlgoirthmThompson();
            StartDFA = StartNDFA;
            Z_DFA = textZ.Text;
            /// TODO

            /// Заносим полученные выходные данные ДКА
            textStartDFA.Text = StartDFA;
            textZDFA.Text = Z_DFA;
            textQDFA.Text = "";

            for (int i = 0; i < Q_DFA.Count; i++)
            {
                if (i != 0)
                {
                    textQDFA.Text += ",";
                }
                textQDFA.Text += "{" + Q_DFA[i] + "}";
            }


            textCommandDFA.Text = "";
            foreach (KeyValuePair<string, Dictionary<int, string>> level in CommandsDFA)
            {
                foreach (KeyValuePair<int, string> sublevel in level.Value)
                {
                    textCommandDFA.Text += "{" + level.Key + "}";
                    textCommandDFA.Text += "-";
                    textCommandDFA.Text += sublevel.Key;
                    textCommandDFA.Text += "-";
                    textCommandDFA.Text += "{" + sublevel.Value + "}";
                    textCommandDFA.Text += "\r\n";
                }
            }
        }

        public void AlgoirthmThompson()
        {
            /// Очередь
            List<string> line = new List<string>();
            /// История
            Q_DFA = new List<string>();

            /// Помещаем стартовое состояние в очередь
            line.Add(StartNDFA);
            Q_DFA.Add(StartNDFA);

            /// пока очередь не пуста
            while (line.Count() != 0)
            {
                /// Достаем из очереди множество, назовем его q
                string q = line.First();
                line.Remove(q);

                // 
                if (q.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Length == 1)
                {
                    Dictionary<int, string> temp;
                    if (CommandsNDFA.TryGetValue(q, out temp))
                    {
                        ///Для всех c
                        foreach (KeyValuePair<int, string> value in temp)
                        {
                            /// Проверяем было ли данное состояние в очереди ранее
                            if (!Q_DFA.Contains(value.Value))
                            {
                                line.Add(value.Value);
                                Q_DFA.Add(value.Value);
                            }

                            ///Добавляем команду в ДКА
                            Dictionary<int, string> tempCommand;
                            if (CommandsDFA.TryGetValue(q, out tempCommand))
                                CommandsDFA.Remove(q);
                            else
                                tempCommand = new Dictionary<int, string>();

                            tempCommand.Add(value.Key, value.Value);
                            CommandsDFA.Add(q, tempCommand);
                        }
                    }
                }
                else
                {
                    Dictionary<int, string> temp = new Dictionary<int, string>();
                    foreach (string q_item in q.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        Dictionary<int, string> tempCommand;
                        if (CommandsNDFA.TryGetValue(q_item, out tempCommand))
                        {
                            ///Для всех c
                            foreach (KeyValuePair<int, string> value in tempCommand)
                            {
                                string state;
                                if (temp.TryGetValue(value.Key, out state))
                                {
                                    temp.Remove(value.Key);
                                    foreach (string vvv in value.Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                                    {
                                        if (!state.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Contains(vvv))
                                            state += "," + vvv;
                                    }
                                }
                                else
                                {
                                    state = value.Value;
                                }

                                // сортировка state
                                if (state.Count() > 1)
                                {
                                    string[] state_str = state.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                    Array.Sort(state_str);
                                    
                                    state = "";
                                    foreach (string s in state_str)
                                    {
                                        state += "," + s;
                                    }
                                    state = state.Substring(1);
                                }

                                temp.Add(value.Key, state);
                            }
                        }
                    }

                    foreach (KeyValuePair<int, string> value in temp)
                    {
                        /// Проверяем было ли данное состояние в очереди ранее
                        if (!Q_DFA.Contains(value.Value))
                        {
                            line.Add(value.Value);
                            Q_DFA.Add(value.Value);
                        }

                        ///Добавляем команду в ДКА
                        Dictionary<int, string> tempCommand;
                        if (CommandsDFA.TryGetValue(q, out tempCommand))
                            CommandsDFA.Remove(q);
                        else
                            tempCommand = new Dictionary<int, string>();

                        tempCommand.Add(value.Key, value.Value);
                        CommandsDFA.Add(q, tempCommand);
                    }
                }
            }
        }

        /// <summary>
        /// Метод преобразует набор команд НДКА, представленный в виде строк, во внутреннее представление программы
        /// </summary>
        /// <param name="value">Набор команд в виде строки состоящей из троек</param>
        /// <param name="z">Алфавит Z</param>
        /// <param name="g">Алфавит Г</param>
        /// <returns>Набор комманд в виде словаря</returns>
        public Dictionary<string, Dictionary<int, string>> InitialCommand(string value, string[] z, string[] g)
        {
            Dictionary<string, Dictionary<int, string>> commands = new Dictionary<string, Dictionary<int, string>>();

            string[] temp = textCommand.Text.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < temp.Count(); i++)
            {
                string[] s = temp[i].Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                if (s.Length == 3 && g.Contains(s[0]) && z.Contains(s[1]))
                {
                    string[] result = s[2].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    for (int j = 0; j < result.Length; j++)
                    {
                        if (!g.Contains(result[j])) throw new Exception("неверно задан набор команд.");
                    }

                    // Удаление повторяющихся исходов
                    IEnumerable<string> res = result.Distinct();
                    if (res.Count() != result.Length)
                    {
                        string ss = "";
                        foreach (string str in res)
                        {
                            ss += str + ",";
                        }
                        ss = ss.Substring(0, ss.Length - 1);
                        temp[i] = s[0] + "-" + s[1] + "-" + ss;
                    }
                }
            }

            /// Перепишем возможые изменения
            textCommand.Text = "";
            foreach (string row in temp)
            {
                textCommand.Text += row + "\r\n";
            }

            // Идем по строкам снова
            for (int i = 0; i < temp.Count(); i++)
            {
                string[] s = temp[i].Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                if (s.Length == 3 && g.Contains(s[0]) && z.Contains(s[1]))
                {
                    string[] result = s[2].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    for (int j = 0; j < result.Length; j++)
                    {
                        if (!g.Contains(result[j])) throw new Exception("неверно задан набор команд.");
                    }

                    Dictionary<int, string> tempCommand;
                    if (commands.TryGetValue(s[0], out tempCommand))
                    {
                        commands.Remove(s[0]);
                    }
                    else
                    {
                        tempCommand = new Dictionary<int, string>();
                    }

                    int a = Int32.Parse(s[1].ToString());

                    tempCommand.Add(a, s[2]);

                    commands.Add(s[0], tempCommand);
                }
                else
                {
                    throw new Exception("неверно задан набор команд.");
                }
            }

            return commands;
        }
    }
}
